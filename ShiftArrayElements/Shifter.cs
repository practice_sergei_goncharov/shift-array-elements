﻿using System;

namespace ShiftArrayElements
{
    public static class Shifter
    {
        /// <summary>
        /// Shifts elements in a <see cref="source"/> array using <see cref="iterations"/> array for getting directions and iterations (see README.md for detailed instructions).
        /// </summary>
        /// <param name="source">A source array.</param>
        /// <param name="iterations">An array with iterations.</param>
        /// <returns>An array with shifted elements.</returns>
        /// <exception cref="ArgumentNullException">source array is null.</exception>
        /// <exception cref="ArgumentNullException">iterations array is null.</exception>
        public static int[] Shift(int[] source, int[] iterations)
        {
            if (source == null)
            {
                throw new ArgumentNullException(nameof(source), "Source array is null.");
            }

            if (iterations == null)
            {
                throw new ArgumentNullException(nameof(iterations), "Directions array is null.");
            }

            if (iterations.Equals(Array.Empty<int>()))
            {
                return source;
            }

            int length = source.Length;
            int[] shiftedArray = new int[length];

            for (int i = 0; i < length; i++)
            {
                shiftedArray[i] = source[i];
            }

            for (int i = 0; i < iterations.Length; i++)
            {
                Direction direction;
                if (i % 2 == 0)
                {
                    direction = Direction.Left;
                }
                else
                {
                    direction = Direction.Right;
                }

                for (int q = 0; q < iterations[i]; q++)
                {
                    if (direction == Direction.Left)
                    {
                        int temp = shiftedArray[0];

                        for (int j = 0; j < length - 1; j++)
                        {
                            shiftedArray[j] = shiftedArray[j + 1];
                        }

                        shiftedArray[length - 1] = temp;
                    }
                    else if (direction == Direction.Right)
                    {
                        int temp = shiftedArray[length - 1];

                        for (int j = length - 1; j > 0; j--)
                        {
                            shiftedArray[j] = shiftedArray[j - 1];
                        }

                        shiftedArray[0] = temp;
                    }
                }
            }

            return shiftedArray;
        }
    }
}
