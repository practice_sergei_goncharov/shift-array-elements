﻿using System;
using System.Linq;

namespace ShiftArrayElements
{
    public static class EnumShifter
    {
        public static int[] Shift(int[] source, Direction[] directions)
        {
            if (source == null)
            {
                throw new ArgumentNullException(nameof(source), "Source array is null.");
            }

            if (directions == null)
            {
                throw new ArgumentNullException(nameof(directions), "Directions array is null.");
            }

            if (directions.Equals(Array.Empty<Direction>()))
            {
                return source;
            }

            if (directions.Any(d => d != Direction.Left && d != Direction.Right))
            {
                throw new InvalidOperationException("Directions array contains an invalid element.");
            }

            int length = source.Length;
            int[] shiftedArray = new int[length];

            for (int i = 0; i < length; i++)
            {
                shiftedArray[i] = source[i];
            }

            for (int i = 0; i < directions.Length; i++)
            {
                Direction direction = directions[i];

                if (direction == Direction.Left)
                {
                    int temp = shiftedArray[0];

                    for (int j = 0; j < length - 1; j++)
                    {
                        shiftedArray[j] = shiftedArray[j + 1];
                    }

                    shiftedArray[length - 1] = temp;
                }
                else if (direction == Direction.Right)
                {
                    int temp = shiftedArray[length - 1];

                    for (int j = length - 1; j > 0; j--)
                    {
                        shiftedArray[j] = shiftedArray[j - 1];
                    }

                    shiftedArray[0] = temp;
                }
            }

            return shiftedArray;
        }
    }
}
